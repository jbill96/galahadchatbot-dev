/*-----------------------------------------------------------------------------
To learn more about this template please visit
https://aka.ms/abs-node-proactive
-----------------------------------------------------------------------------*/
"use strict";
var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var azure = require('azure-storage');
var path = require('path');
var https = require('https');
var date = require('date-and-time');
var fs = require('fs');
var uuid = require('uuid');
var async = require('async');
var config = require('./config.js');
require('dotenv').config();
var apiai = require('./apiai.js');
var recognizer = new apiai(process.env.APIAI_TOKEN);
var translator = require('./translator.js');
var util = require('util');
var restify = require('restify');
var express = require('express');
var winston = require('winston');
var logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console()
    ]
});
var intents = new builder.IntentDialog({
    recognizers: [recognizer]
});

const SITE = 'https://galahadchat.azurewebsites.net/images/%s.png';


// Setup Restify Server
var server = express();
server.use('/images', express.static(__dirname + '/images'));
server.use('/retailbot', express.static(__dirname + '/retailbot'));
server.listen(process.env.port || process.env.PORT || 3978, function () {
    logger.info('started server', {
        port: 3978
    });
});


// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    stateEndpoint: process.env.BotStateEndpoint,
    openIdMetadata: process.env.BotOpenIdMetadata
});

server.post('/api/messages', connector.listen());

var bot = new builder.UniversalBot(connector);

//Sample image names for browsing
var items = ['backpacks', 'boots', 'dresses', 'handbags', 'hats', 'jackets', 'jeans', 'pants',
    'pantsuits', 'sandals', 'shorts', 'suits', 'sweaters', 'tops', 'tshirts'
];

// Handle message from user
bot.dialog('/', intents);

// resets a user's session
intents.matches('delete data', (session, args) => {
    logger.warn('reseting session');
    session.userData = {};
    session.conversationData = {}
    session.privateConversationData = {}
    session.send('RESET SESSION');
});

// initial greetings
intents.matches('domains', function (session, args) {
    logger.info('MATCHED DOMAINS');
    console.log(args.messages);
    logger.info({
        intent: 'domains',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});
// initial greetings
intents.matches('smalltalk.greetings.hello', function (session, args) {
    logger.info({
        intent: 'smalltalk.greetings.hello',
        messages: args.messages,
        lang: args.lang
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('order_cancel', function (session, args) {
    logger.info({
        intent: 'order_cancel',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel - no', function (session, args) {
    logger.info({
        intent: 'order_cancel - no',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel - yes', function (session, args) {
    logger.info({
        intent: 'order_cancel - yes',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel_followup - no', function (session, args) {
    logger.info({
        intent: 'order_cancel_followup - no',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('order_cancel_followup - yes', function (session, args) {
    logger.info({
        intent: 'order_cancel_followup - yes',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request', function (session, args) {
    session.userData.lang = args.lang;
    logger.info({
        intent: 'fashion_advice_request',
        messages: args.messages
    });
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - reject_advice', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - reject_advice',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - cancel_request', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - cancel_request',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - persona - cancel', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - persona - cancel',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request_followup_question - clothing_type - cancel', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request_followup_question - clothing_type - cancel',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request_followup_question - cancel', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request_followup_question - cancel',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});
/*
//browse_items intent: create HeroCards with local images if we have them or images from pixabay.com
intents.matches('browse_items', function (session, args) {
    
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        session.send(messages[i]);
    }
    if (args.entities[1].entity == false) {
        var msg = new builder.Message(session);
        msg.attachmentLayout(builder.AttachmentLayout.carousel);
        var imageURL = "";
        var card = {};
        if (items.includes(args.params.item.toLowerCase())) {
            for (var i = 1; i < 4; i++) {
                imageURL = path.join(__dirname, "images/" + args.params.item + i + ".jpg");
                card = new builder.HeroCard(session)
                    //.images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.imBack(session, "Buy " + args.parameters['item-original'] + " #" + i, "Buy")
                    ]);
                msg.addAttachment(card);
            }
            session.sendTyping();
            session.send(msg);
        } else {
            var API_KEY = '5767102-e1ca99133bdfd3d3d645479a8';
            var URL = "https://pixabay.com/api/?key=" + API_KEY + "&q=" + encodeURIComponent(args.params["item"]) + "&image_type=photo&safesearch=true";
            var jsonResp = "";
            var jsonObj = {};
            https.get(URL, (res) => {
                res.on('data', (d) => {
                    jsonResp = jsonResp + d.toString();
                }).on('end', () => {
                    try {
                        jsonObj = JSON.parse(jsonResp);
                    } catch (err) {
                        console.log('parse error');
                    }
                    session.sendTyping();
                    for (var i = 1; i < 4 && i < jsonObj['hits'].length; i++) {
                        imageURL = jsonObj['hits'][i]['webformatURL'];
                        console.log(imageURL);
                        card = new builder.HeroCard(session)
                            //.images([builder.CardImage.create(session, imageURL)])
                            .buttons([
                                builder.CardAction.imBack(session, "Buy " + args.params['item-original'] + " #" + i, "Buy")
                            ]);
                        msg.addAttachment(card);
                    }
                    console.log(msg);
                    session.sendTyping();
                    session.send(msg);
                });
            }).on('error', (err) => {
                console.error(err);
            });
        }
    }
});
*/

//Display existing fashion profile if it exists. Otherwise, start fashion advice dialog.
intents.matches('fashion_advice_request - allow_followup_questions', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - allow_followup_questions',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var updateProfile = false;
    if (session.userData.hasOwnProperty('fashionProfile')) {
        session.send(args.config_lang.existing_profile_prompt);
        var url = util.format(SITE, session.userData.fashionProfile.persona);
        var brands = "";
        var budget = "None";
        var obsession = "None";
        if (session.userData.fashionProfile.hasOwnProperty('budget')) {
            budget = session.userData.fashionProfile.budget + args.config_lang.orless;
        }
        if (session.userData.fashionProfile.hasOwnProperty('obsession')) {
            console.log(session.userData.fashionProfile);
            obsession = args.config_lang[session.userData.fashionProfile.obsession.toLowerCase()]
        }
        if (session.userData.fashionProfile.hasOwnProperty("brands")) {
            for (var i in session.userData.fashionProfile.brands) {
                brands = brands + session.userData.fashionProfile.brands[i] + ", ";
            }
        }
        var msg = new builder.Message(session)
            .speak(args.config_lang.speak.look_at_fashion_profile)
            .addAttachment({
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    type: "AdaptiveCard",
                    body: [{
                            "type": "Image",
                            "url": url,
                            "size": "large",
                            "horizontalAlignment": "left"
                        },
                        {
                            "type": "FactSet",
                            "facts": [{
                                    "title": args.config_lang.favorite_brands,
                                    "value": brands
                                },
                                {
                                    "title": args.config_lang.fashion_obsession,
                                    "value": obsession
                                },
                                {
                                    "title": args.config_lang.budget,
                                    "value": budget
                                }
                            ]
                        }
                    ]
                }
            });
        session.send(msg);
        var msg = new builder.Message(session);
        msg.text(args.config_lang.update_question);
        msg.speak(args.config_lang.speak.update_question);
        var card = new builder.HeroCard(session)
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.update_profile_text, args.config_lang.update_profile_text),
                builder.CardAction.imBack(session, args.config_lang.existing_profile_text, args.config_lang.existing_profile_text)
            ]);
        msg.addAttachment(card);
        session.send(msg).endDialog();
    } else {

        var messages = args.messages;
        var msg = new builder.Message(session);
        var card = new builder.HeroCard(session)
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.casual, args.config_lang.casual),
                builder.CardAction.imBack(session, args.config_lang.professional, args.config_lang.professional),
                builder.CardAction.imBack(session, args.config_lang.seasonal, args.config_lang.seasonal),
                builder.CardAction.imBack(session, args.config_lang.athletic, args.config_lang.athletic),
                builder.CardAction.imBack(session, args.config_lang.formal, args.config_lang.formal)
            ]);
        msg.text(messages);
        msg.speak(messages + args.config_lang.speak.type_choices);
        msg.addAttachment(card);
        session.send(msg).endDialog();
    }
});

//Start fashion advice dialog for user updating an existing profile
intents.matches('fashion_advice_request - update_profile - yes', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - update_profile - yes',
    });
    session.userData.lang = args.lang;
    session.userData.fashionProfile.update = true;
    var msg = new builder.Message(session);

    var card = new builder.HeroCard(session)
        .buttons([
            builder.CardAction.imBack(session, args.config_lang.casual, args.config_lang.casual),
            builder.CardAction.imBack(session, args.config_lang.professional, args.config_lang.professional),
            builder.CardAction.imBack(session, args.config_lang.seasonal, args.config_lang.seasonal),
            builder.CardAction.imBack(session, args.config_lang.athletic, args.config_lang.athletic),
            builder.CardAction.imBack(session, args.config_lang.formal, args.config_lang.formal)
        ]);

    msg.text(args.config_lang.shopping_today_question);
    msg.speak(args.config_lang.shopping_today_question + args.config_lang.speak.type_choices);
    msg.addAttachment(card);
    msg.inputHint(builder.InputHint.acceptingInput);
    session.send(msg).endDialog();

});

//Give outfit recommendation for user that does not want to update their profile
intents.matches('fashion_advice_request - update_profile - no', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - update_profile - no',
    });
    session.userData.lang = args.lang;
    session.send(args.config_lang.recommend_existing_text);
    session.userData.fashionProfile.update = false;
    var msg = new builder.Message(session);
    var url = util.format(SITE, session.userData.fashionProfile.persona + "Full" + session.userData.recNum);
    var card = new builder.HeroCard(session)
        .images([builder.CardImage.create(session, url)])
        .buttons([
            builder.CardAction.imBack(session, args.config_lang.like, args.config_lang.like),
            builder.CardAction.imBack(session, args.config_lang.dislike, args.config_lang.dislike)
        ]);
    msg.addAttachment(card);
    session.send(msg).endDialog();
});

//If user is updating profile, move to budget. Otherwise, continue with fashion personas.
intents.matches('fashion_advice_request_followup_question - clothing_type', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request_followup_question - clothing_type',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    if (args.entities[1].entity == false) {
        if (session.userData.hasOwnProperty('fashionProfile') &&
            session.userData.fashionProfile.hasOwnProperty('update') &&
            !session.userData.fashionProfile.update) {
            var msg = new builder.Message(session)
                .text(messages[0])
                .speak(messages[0]);
            session.send(msg).endDialog();
            var msg = new builder.Message(session)
                .text(args.config_lang.budget_question)
                .speak(args.config_lang.budget_question);

            /*var card = new builder.HeroCard(session)
                .buttons([
                            builder.CardAction.imBack(session, "<$50", "<$50"),
                            builder.CardAction.imBack(session, "$51-$100", "$51-$100"),
                            builder.CardAction.imBack(session, "$101-$150", "$101-$150"),
                            builder.CardAction.imBack(session, "$151-$300", "$151-$300"),
                            builder.CardAction.imBack(session, "$301+", "$301+")
                        ]
                    );
            msg.addAttachment(card);
            */
            session.send(msg);
        } else {

            for (var i = 0; i < messages.length; i++) {
                var msg = new builder.Message(session)
                    .text(messages[i])
                    .speak(messages[i]);
                session.send(msg).endDialog();
            }
            var names = [args.config_lang.names.jenny, args.config_lang.names.julia, args.config_lang.names.rebecca, args.config_lang.names.mary];
            var pic_names = [config.en.names.jenny, config.en.names.julia, config.en.names.rebecca, config.en.names.mary];
            var msg = new builder.Message(session)
                .attachmentLayout(builder.AttachmentLayout.carousel);
            for (var i = 0; i < pic_names.length; i++) {
                msg.addAttachment(makeCard(session, names[i], pic_names[i]));
            }
            msg.inputHint(builder.InputHint.acceptingInput);
            session.sendBatch();
            session.send(msg).endDialog();
        }
    } else {
        var msg = new builder.Message(session)
            .text(messages[0])
            .speak(messages[0] + args.config_lang.speak.type_choices);
        var card = new builder.HeroCard(session)
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.casual, args.config_lang.casual),
                builder.CardAction.imBack(session, args.config_lang.professional, args.config_lang.professional),
                builder.CardAction.imBack(session, args.config_lang.seasonal, args.config_lang.seasonal),
                builder.CardAction.imBack(session, args.config_lang.athletic, args.config_lang.athletic),
                builder.CardAction.imBack(session, args.config_lang.formal, args.config_lang.formal)
            ]);
        msg.addAttachment(card);
        msg.inputHint(builder.InputHint.acceptingInput)
        session.send(msg).endDialog();
    }
});

function makeCard(session, name, pic_name) {
    return new builder.HeroCard(session)
        .title(name)
        .images([builder.CardImage.create(session, util.format(SITE, pic_name))])
        .buttons([
            builder.CardAction.imBack(session, name, name)
        ]);
}

//Prompt user for fashion obsession
intents.matches('fashion_advice_request - persona', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - persona',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    if (args.entities[1].entity == false) {
        if (!session.userData.hasOwnProperty('fashionProfile')) {
            session.userData.fashionProfile = {};
        }
        session.userData.fashionProfile.persona = args.parameters.persona;
        var msg = new builder.Message(session);
        msg.text(messages[0])
        msg.speak(messages[0] + args.config_lang.speak.persona_choices)
        var card = new builder.HeroCard(session)
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.elegant, args.config_lang.elegant),
                builder.CardAction.imBack(session, args.config_lang.urban, args.config_lang.urban),
                builder.CardAction.imBack(session, args.config_lang.minimalism, args.config_lang.minimalism),
                builder.CardAction.imBack(session, args.config_lang.classic, args.config_lang.classic),
                builder.CardAction.imBack(session, args.config_lang.none, args.config_lang.none)
            ]);
        msg.addAttachment(card);
        session.send(msg);
    } else {
        for (var i = 0; i < messages.length; i++) {
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i]);
            if (i == messages.length - 1) {
                msg.inputHint = builder.InputHint.acceptingInput;
            }
            session.send(msg).endDialog();
        }
    }
});

//Prompt user for budget
intents.matches('fashion_advice_request - brands', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - brands',
        messages: args.messages
    });
    
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
    session.userData.fashionProfile.brands = args.parameters.brands;
    var msg = new builder.Message(session)
        .text(util.format(args.config_lang.favorite_brands_return_text, args.parameters.brands.toString()))
        .speak(util.format(args.config_lang.favorite_brands_return_text, args.parameters.brands.toString()));
    session.send(msg).endDialog();
    var msg = new builder.Message(session)
        .text(args.config_lang.budget_question)
        .speak(args.config_lang.budget_question);
    session.send(msg).endDialog();


    /*var msg = new builder.Message(session)
        .text(util.format(args.config_lang.favorite_brands_return_text, args.parameters.brands.toString()))
        .speak(util.format(args.config_lang.favorite_brands_return_text, args.parameters.brands.toString()))
        .addAttachment({
            "contentType": "application/vnd.microsoft.card.adaptive",
            "content": {
                "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                "type": "AdaptiveCard",
                "version": "1.0",
                "speak": "<s>" + args.config_lang.budget_question + "</s>",
                "body": [{
                        "type": "TextBlock",
                        "text": args.config_lang.budget_question,
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "budget",
                        "isMultiSelect": false,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "$0-$50",
                                "value": "$0-$50"
                            },
                            {
                                "title": "$51-$100",
                                "value": "$51-$100"
                            },
                            {
                                "title": "$101-$150",
                                "value": "$101-$150"
                            },
                            {
                                "title": "$151-$300",
                                "value": "$151-$300"
                            },
                            {
                                "title": "$301+",
                                "value": "$301+"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": args.config_lang.select,
                    "data": {
                        "lang": args.lang
                    }
                }]
            }
        });
        */

});

//Give first suggestion like/dislike HeroCard
intents.matches('fashion_advice_request - obsession', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - obsession',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }

    session.userData.fashionProfile.obsession = args.parameters.obsession;
    var imageNum = Math.floor(Math.random() * (3 - 1)) + 1;
    var msg = new builder.Message(session);
    var persona = session.userData.fashionProfile.persona;
    var url = util.format(SITE, session.userData.fashionProfile.persona + "_top" + imageNum);

    var card = new builder.HeroCard(session)
        .images([builder.CardImage.create(session, url)])
        .buttons([
            builder.CardAction.imBack(session, args.config_lang.like, args.config_lang.like),
            builder.CardAction.imBack(session, args.config_lang.dislike, args.config_lang.dislike)
        ]);
    msg.addAttachment(card);
    session.sendTyping();
    session.send(msg);

});

//Give second suggestion like/dislike HeroCard
intents.matches('fashion_advice_request - obsession_like_dislike(1)', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - obsession_like_dislike(1)',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    if (args.entities[1].entity == false) {
        var msg = new builder.Message(session)
            .text(messages[0])
            .speak(messages[0]);
        session.send(msg).endDialog();

        var msg = new builder.Message(session);
        var imageNum = Math.floor(Math.random() * (3 - 1)) + 1;
        var persona = session.userData.fashionProfile.persona;
        var url = util.format(SITE, session.userData.fashionProfile.persona + "_Pants" + imageNum);

        var card = new builder.HeroCard(session)
            .images([builder.CardImage.create(session, url)])
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.like, args.config_lang.like),
                builder.CardAction.imBack(session, args.config_lang.dislike, args.config_lang.dislike)
            ]);
        msg.addAttachment(card);
        session.sendTyping();
        session.send(msg);
    } else {
        for (var i = 0; i < messages.length; i++) {
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i]);
            if (i == messages.length - 1) {
                msg.inputHint = builder.InputHint.acceptingInput;
            }
            session.send(msg).endDialog();
        }
    }
});

//Give third suggestion like/dislike HeroCard
intents.matches('fashion_advice_request - obsession_like_dislike(2)', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - obsession_like_dislike(2)',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    if (args.entities[1].entity == false) {
        var msg = new builder.Message(session)
            .text(messages[0])
            .speak(messages[0]);
        session.send(msg).endDialog();

        var msg = new builder.Message(session);
        var imageNum = Math.floor(Math.random() * (3 - 1)) + 1;
        var url = util.format(SITE, session.userData.fashionProfile.persona + "_bag" + imageNum);
        var card = new builder.HeroCard(session)
            .images([builder.CardImage.create(session, url)])
            .buttons([
                builder.CardAction.imBack(session, args.config_lang.like, args.config_lang.like),
                builder.CardAction.imBack(session, args.config_lang.dislike, args.config_lang.dislike)
            ]);
        msg.addAttachment(card);
        session.sendTyping();
        session.send(msg);
    } else {
        for (var i = 0; i < messages.length; i++) {
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i]);
            if (i == messages.length - 1) {
                msg.inputHint = builder.InputHint.acceptingInput;
            }
            session.send(msg).endDialog();
        }
    }
});

//Send fashion brands checklist as AdaptiveCard
intents.matches('fashion_advice_request - obsession_like_dislike(3)', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - obsession_like_dislike(3)',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }

    // toggle between using card vs voice

    var msg = new builder.Message(session)
        .text(args.config_lang.brand_question)
        .speak(args.config_lang.brand_question);
    msg.inputHint = builder.InputHint.acceptingInput;
    session.send(msg).endDialog();

    var msg = new builder.Message(session)
        .addAttachment({
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                type: "AdaptiveCard",
                "body": [{
                        "type": "TextBlock",
                        "text": args.config_lang.brand_question,
                        "size": "large"
                    },
                    {
                        "type": "Input.ChoiceSet",
                        "id": "brands",
                        "isMultiSelect": true,
                        "isRequired": true,
                        "style": "compact",
                        "choices": [{
                                "title": "Adidas",
                                "value": "Adidas"
                            },
                            {
                                "title": "Calvin Klein",
                                "value": "Calvin Klein"
                            },
                            {
                                "title": "Chanel",
                                "value": "Chanel"
                            },
                            {
                                "title": "Christian Dior",
                                "value": "Christian Dior"
                            },
                            {
                                "title": "Dockers",
                                "value": "Dockers"
                            },
                            {
                                "title": "Dolce & Gabbana",
                                "value": "Dolce & Gabbana"
                            },
                            {
                                "title": "Donna Karan",
                                "value": "Donna Karan"
                            },
                            {
                                "title": "Eddie Bauer",
                                "value": "Eddie Bauer"
                            },
                            {
                                "title": "Esprit",
                                "value": "Esprit"
                            },
                            {
                                "title": "Fila",
                                "value": "Fila"
                            },
                            {
                                "title": "Giorgio Armani",
                                "value": "Giorgio Armani"
                            },
                            {
                                "title": "Gucci",
                                "value": "Gucci"
                            },
                            {
                                "title": "Guess",
                                "value": "Guess"
                            },
                            {
                                "title": "Hugo Boss",
                                "value": "Hugo Boss"
                            },
                            {
                                "title": "J. Crew",
                                "value": "J. Crew"
                            },
                            {
                                "title": "Izod",
                                "value": "Izod"
                            },
                            {
                                "title": "Jennifer Lopez",
                                "value": "Jennifer Lopez"
                            },
                            {
                                "title": "Kenneth Cole",
                                "value": "Kenneth Cole"
                            },
                            {
                                "title": "Lacoste",
                                "value": "Lacoste"
                            },
                            {
                                "title": "Land's End",
                                "value": "Land's End"
                            },
                            {
                                "title": "Laura Ashley",
                                "value": "Laura Ashley"
                            },
                            {
                                "title": "Lee",
                                "value": "Lee"
                            },
                            {
                                "title": "Levi's",
                                "value": "Levi's"
                            },
                            {
                                "title": "Liz Claiborne",
                                "value": "Liz Claiborne"
                            },
                            {
                                "title": "L.L Bean",
                                "value": "L.L Bean"
                            },
                            {
                                "title": "Lucky Brand Jeans",
                                "value": "Lucky Brand Jeans"
                            },
                            {
                                "title": "Mango",
                                "value": "Mango"
                            },
                            {
                                "title": "Marc Jacobs",
                                "value": "Marc Jacobs"
                            },
                            {
                                "title": "Michael Kors",
                                "value": "Michael Kors"
                            },
                            {
                                "title": "Mudd",
                                "value": "Mudd"
                            },
                            {
                                "title": "Nicole Miller",
                                "value": "Nicole Miller"
                            },
                            {
                                "title": "Nike",
                                "value": "Nike"
                            },
                            {
                                "title": "Prada",
                                "value": "Prada"
                            },
                            {
                                "title": "Puma",
                                "value": "Puma"
                            },
                            {
                                "title": "Ralph Lauren",
                                "value": "Ralph Lauren"
                            },
                            {
                                "title": "Timberland",
                                "value": "Timberland"
                            },
                            {
                                "title": "Tommy Hilfiger",
                                "value": "Tommy Hilfiger"
                            },
                            {
                                "title": "Van Heusen",
                                "value": "Van Heusen"
                            },
                            {
                                "title": "Versace",
                                "value": "Versace"
                            },
                            {
                                "title": "Yves Saint Laurent",
                                "value": "Yves Saint Laurent"
                            }
                        ]
                    }
                ],
                "actions": [{
                    "type": "Action.Submit",
                    "title": args.config_lang.select,
                    "data": {
                        "lang": args.lang,
                        "translate": args.translate
                    }
                }]
            }
        });
    session.send(msg);
});

//Give outfit suggestion at the end of fashion questaions
intents.matches('fashion_advice_request - budget', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - budget',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
    session.userData.recNum = 1;
    var msg = new builder.Message(session)
        .text(args.config_lang.generate_recommendation_text)
        .speak(args.config_lang.generate_recommendation_text);
    session.send(msg).endDialog();
    session.userData.recNum = 1;
    session.userData.fashionProfile.budget = args.parameters.budget;
    var msg = new builder.Message(session);
    var url = util.format(SITE, session.userData.fashionProfile.persona + "Full" + session.userData.recNum);
    var card = new builder.HeroCard(session)
        .images([builder.CardImage.create(session, url)])
        .buttons([
            builder.CardAction.imBack(session, args.config_lang.like, args.config_lang.like),
            builder.CardAction.imBack(session, args.config_lang.dislike, args.config_lang.dislike)
        ]);
    msg.addAttachment(card);
    session.send(args.config_lang.found_outfit);
    session.sendBatch();
    session.userData.recNum = 2;
    session.send(msg);
    session.sendBatch();
});

//Save user dislike response to outfit suggestion
intents.matches('fashion_advice_request - budget_followup_suggestion - dislike', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - budget_followup_suggestion - dislike',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
    session.userData.fashionProfile.suggestionApproval = false;
});

//Save user like response to outfit suggestion
intents.matches('fashion_advice_request - budget_followup_suggestion - like', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - budget_followup_suggestion - like',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
    session.userData.fashionProfile.suggestionApproval = true;
});

intents.matches('fashion_advice_request - budget_followup_suggestion - add_cart - yes', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - budget_followup_suggestion - add_cart - yes',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - budget_followup_suggestion - add_cart - no', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - budget_followup_suggestion - add_cart - no',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('order_change', function (session, args) {
    logger.info({
        intent: 'order_change',
        messages: args.messages
    });

    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
});

//Summarize order change
intents.matches('order_change - yes - change', function (session, args) {
    logger.info({
        intent: 'order_change - yes - change',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var response = args.config_lang.change_order_to;
    for (var param in args.params) {
        response = response + args.params[param];
    }
    response += ".";
    session.send(response);

    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i]);
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
});

//Fill in and prompt for necessary info for question about products
intents.matches('item_inquiry', function (session, args) {
    logger.info({
        intent: 'item_inquiry',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var sizes = "";
    var followupQuestion = "";
    /*if (args.parameters.size != "") {
        session.conversationData.size = args.parameters.size;
        sizes += args.config_lang.size + args.parameters.size;
        if (args.parameters.fit != "") {
            sizes += args.config_lang.and + args.parameters.fit;
            session.conversationData.fit = args.parameters.fit;
            followupQuestion = args.config_lang.what_color;
        }
        if (args.parameters.color != "") {
            session.conversationData.color = args.parameters.color;
            sizes += args.config_lang.and + args.parameters.color;
            followupQuestion = util.format(args.config_lang.add_cart_question, args.parameters.item);
        }
    } else if (args.parameters.fit != "") {
        session.conversationData.fit = args.parameters.fit;
        sizes += args.parameters.fit;
        followupQuestion = args.config_lang.what_size;
        if (args.parameters.color != "") {
            session.conversationData.color = args.parameters.color;
            sizes += args.config_lang.and + args.parameters.color;
        }
    } else if (args.parameters.color != "") {
        session.conversationData.color = args.parameters.color;
        followupQuestion = args.config_lang.what_size;
        sizes += args.parameters.color;
    } else {
        session.send(args.config_lang.what_size);
        return;
    }
    var item = args.parameters.item;
    if (args.parameters.hasOwnProperty("brands")) {
        item = args.parameters.brands + " " + item;
    }*/
    session.send(args.config_lang.what_size);
    //session.send(util.format(args.config_lang.yes_item_size, item, sizes));
    //session.send(followupQuestion);
});

//If color was not filled in, prompt for it. Otherwise, proceed.
intents.matches('item_inquiry - size', function (session, args) {
    logger.info({
        intent: 'item_inquiry - size',
    });
    session.userData.lang = args.lang;
    /*if (!session.conversationData.hasOwnProperty('color')) {
        session.send(args.config_lang.what_color);
    } else {

        var messages = args.messages;
        for (var i = 0; i < messages.length; i++) {
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i]);
            if (i == messages.length - 1) {
                msg.inputHint = builder.InputHint.acceptingInput;
            }
            session.send(msg).endDialog();
        }
    }*/
    session.send(args.config_lang.what_color);
});

//Summary of product question findings
intents.matches('item_inquiry - color', function (session, args) {
    logger.info({
        intent: 'item_inquiry - color',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    var brand = "";
    var item = "";
    var color = "";
    var size = "";
    for (var i = 0; i < args.contexts.length; i++) {
        if (args.contexts[i].name == "sales_support-color-followup-2") {
            console.log(args.contexts[i]);
            brand = args.contexts[i].parameters.brand;
            item = args.contexts[i].parameters.item;
            color = args.contexts[i].parameters.color;
            size = args.contexts[i].parameters.size;
            if (args.lang === 'en') {
                item = item;
                color = color;
                size = size;
                break;
            } else {
                item = args.config_lang.items[args.contexts[i].parameters.item];
                color = args.config_lang.colors[args.contexts[i].parameters.color];
                size = args.config_lang.sizes[args.contexts[i].parameters.size.match(/\d+/)[0]];
                break;
            }
        }
    }
    session.send(util.format(args.config_lang.sales_support_followup, brand, item, color, size));
});


intents.matches('item_inquiry - color - no', function (session, args) {
    logger.info({
        intent: 'item_inquiry - color - no',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('item_inquiry_followup - no', function (session, args) {
    logger.info({
        intent: 'item_inquiry_followup - no',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

function send_messages(session, messages){
    if(messages){
        for(var i = 0 ; i < messages.length ; i++){
            var msg = new builder.Message(session)
                .text(messages[i])
                .speak(messages[i])
                .inputHint(builder.InputHint.acceptingInput);
            session.send(msg);
        }
    }
}

intents.matches('item_inquiry_followup - yes', function (session, args) {
    logger.info({
        intent: 'item_inquiry_followup - yes',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('item_inquiry - color - yes', function (session, args) {
    logger.info({
        intent: 'item_inquiry - color - yes',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

//Give suggested recurring responses
intents.matches('fashion_advice_request - recurring_suggestions - yes', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - recurring_suggestions - yes',
        messages: args.messages
    });
    session.userData.lang = args.lang;

    var messages = args.messages;
    var msg = new builder.Message(session);

    msg.text(messages[0])
    msg.speak(messages[0] + args.config_lang.speak.recurring_choices)
    var card = new builder.HeroCard(session)
        .buttons([
            builder.CardAction.imBack(session, args.config_lang.daily, args.config_lang.daily),
            builder.CardAction.imBack(session, args.config_lang.weekly, args.config_lang.weekly),
            builder.CardAction.imBack(session, args.config_lang.monthly, args.config_lang.monthly),
            builder.CardAction.imBack(session, args.config_lang.occasionally, args.config_lang.occasionally)
        ]);
    msg.addAttachment(card);
    session.send(msg).endDialog();
});

intents.matches('fashion_advice_request - recurring_suggestions - no', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - recurring_suggestions - no',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - recurring_suggestions - frequency_received', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - recurring_suggestions - frequency_received',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - additional_help - no', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - additional_help - no',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice_request - additional_help - yes', function (session, args) {
    logger.info({
        intent: 'fashion_advice_request - additional_help - yes',
        messages: args.messages
    });
    session.userData.lang = args.lang;
    send_messages(session, args.messages);
});

intents.matches('fashion_advice-add-to-cart', function (session, args) {
    session.userData.lang = args.lang;
    var messages = args.messages;
    for (var i = 0; i < messages.length; i++) {
        var msg = new builder.Message(session)
            .text(messages[i])
            .speak(messages[i])
        if (i == messages.length - 1) {
            msg.inputHint = builder.InputHint.acceptingInput;
        }
        session.send(msg).endDialog();
    }
});

//Default sending of api.ai responses. Handle brands list submit action.
intents.onDefault(function (session, args) {
    session.userData.lang = args.lang;
    console.log(JSON.stringify(args));
    if (args.intent != "None") {

        var messages = args.messages;
        for (var i = 0; i < messages.length; i++) {
            session.send(messages[i])
        }
    } else {
        console.log("Brands list: " + JSON.stringify(session.message.value));
        // TODO
        // need to use generic prompt (favorite_brands_return_text)
        session.send(session.message.value.brands.replace(/;/g, ", "));
        var msg = new builder.Message(session)
            .text(args.config_lang.budget_question)
            .speak(args.config_lang.budget_question);
        var card = new builder.HeroCard(session)
            .buttons([
                builder.CardAction.imBack(session, "<$50", "<$50"),
                builder.CardAction.imBack(session, "$51-$100", "$51-$100"),
                builder.CardAction.imBack(session, "$101-$150", "$101-$150"),
                builder.CardAction.imBack(session, "$151-$300", "$151-$300"),
                builder.CardAction.imBack(session, "$301+", "$301+")
            ]);
        msg.addAttachment(card);
        session.userData.fashionProfile.brands = session.message.value.brands.replace(/;/g, ",").split(",");
        session.send(msg);
    }
});