"use strict";
var apiai = require('apiai-promise');
var uuid = require('uuid');
var translator = require('./translator.js');
var async = require('async');
var config = require('./config.js');

var ApiAiRecognizer = function (token) {
    this.app = apiai(token);
    this._onEnabled = [];
    this._onFilter = [];
};

ApiAiRecognizer.prototype.recognize = function (context, done) {
    var _this = this;

    this.isEnabled(context, function (err, enabled) {
        if (err) {
            callback(err, null);
        } else if (!enabled) {
            callback(null, {
                score: 0.0,
                intent: null
            });
        } else {
            //console.log('BEGIN CONTEXT');
            //console.log(context);
            var intent = {
                score: 0.0
            };
            try {
                var sessionId = context.message.address.user.id + context.message.address.channelId;
                if (sessionId.length > 36) {
                    sessionId = sessionId.slice(0, 35);
                }

            } catch (err) {
                var sessionId = uuid();
            }
            if (context.message.text && !context.message.value) {
                async.waterfall([
                    function (callback) {
                        translator.detect(context.message.text, callback);
                    },
                    function (lang, callback) {
                        intent.from = lang;
                        if (lang !== 'en') {
                            translator.translate({
                                text: context.message.text,
                                from: lang,
                                to: 'en'

                            }, callback);
                        } else {
                            callback(null, context.message.text, lang);
                        }
                    },
                    function (message, lang, callback) {
                        _this.app.textRequest(message.toLowerCase(), {
                                sessionId: sessionId
                            })
                            .then((response) => {
                                var result = response.result;

                                /*
                                console.log('********************************');
                                console.log('RESULT');
                                console.log(result);
                                console.log('********************************');
                                console.log('PARAMS');
                                console.log(result.parameters);
                                console.log('********************************');
                                console.log('MESSAGES');
                                console.log(result.fulfillment.messages);
                                console.log('********************************');
                                console.log('CONTEXT');
                                console.log(context);
                                console.log('********************************');
                                */
                                intent = handle_apiai_result(intent, result);
                                intent.contexts = result.contexts;
                                callback(null, intent, lang, context.userData.lang);
                            }).catch((err) => {
                                console.log(err);
                            });
                    },
                    final_translation
                ], (err, results) => {
                    if (err) {
                        console.log(err);
                        done(err);
                    }
                    /*
                    console.log('********************************');
                    console.log('RESULTS');
                    console.log(results);
                    console.log('********************************');
                    */
                    done(null, results);
                });
            } else if (context.message.value) {
                var value = context.message.value;
                if (value.brands) {
                    //console.log('submitting brands');
                    var brands = context.message.value.brands;
                    var brand_array = brands.split(';').map((val) => {
                        return val;
                    });
                    var config_lang = context.userData.lang === 'en' ? config.en : config.ch;
                    intent = {
                        score: 1,
                        intent: 'fashion_advice_request - brands',
                        parameters: {
                            brands: brand_array
                        },
                        lang: value.lang,
                        config_lang: config_lang
                    };
                    done(null, intent);
                } else if (value.budget) {
                    console.log('submitting budget');
                    var budget = value.budget;
                    var config_lang = value.translate ? config.ch : config.en;
                    async.waterfall([
                        function (callback) {
                            _this.app.textRequest(budget, {
                                    sessionId: sessionId
                                })
                                .then((response) => {
                                    var result = response.result;
                                    intent = handle_apiai_result(intent, result);
                                    intent.config_lang = config_lang;
                                    intent.parameters = {
                                        budget: budget
                                    };
                                    callback(null, intent, value.lang, context.userData.lang);
                                }).catch((err) => {
                                    callback(err);
                                });
                        },
                        final_translation
                    ], (err, results) => {
                        done(null, results);
                    });
                }
            } else {
                intent = {
                    score: 1,
                    intent: "None",
                    entities: []
                };
                done(null, intent);
            }
        }
    });
}

function get_messages(messages) {
    var messages_list = [];
    messages.forEach((msg) => {
        messages_list.push(msg.speech);
    });
    return messages_list;
}

function handle_apiai_result(intent, result) {
    if (result.source == 'domains') {
        entities_found = [{
                entity: result.fulfillment.speech,
                type: 'fulfillment',
                score: 1
            },
            {
                entity: result.actionIncomplete,
                type: 'actionIncomplete',
                score: 1
            }
        ];
        intent = {
            score: result.score,
            intent: result.source,
            entities: entities_found,
        };
    } else if (result.source == 'agent') {
        var entities_found = [{
                entity: result.fulfillment.speech,
                messages: result.fulfillment.messages,
                type: 'fulfillment',
                score: 1
            },
            {
                entity: result.actionIncomplete,
                type: 'actionIncomplete',
                score: 1
            }
        ];
        intent = {
            score: result.score,
            intent: result.metadata.intentName,
            entities: entities_found,
            parameters: result.parameters,
        };
    }
    intent.messages = get_messages(result.fulfillment.messages);
    return intent;
}

function final_translation(intent, lang, user_lang, callback) {
    if (lang !== 'en' && lang == 'zh-CHS') {
        intent.lang = lang;
        intent.config_lang = config.ch;
        translator.translate_array(intent, lang, callback);
    } else {
        if (user_lang === 'zh-CHS') {
            intent.lang = user_lang;
            intent.config_lang = config.ch;
            callback(null, intent);
        } else {
            intent.lang = 'en';
            intent.config_lang = config.en;
            callback(null, intent);
        }
    }
}

ApiAiRecognizer.prototype.onEnabled = function (handler) {
    this._onEnabled.unshift(handler);
    return this;
};

ApiAiRecognizer.prototype.onFilter = function (handler) {
    this._onFilter.push(handler);
    return this;
};

ApiAiRecognizer.prototype.isEnabled = function (context, callback) {
    var index = 0;
    var _that = this;

    function next(err, enabled) {
        if (index < _that._onEnabled.length && !err && enabled) {
            try {
                _that._onEnabled[index++](context, next);
            } catch (e) {
                callback(e, false);
            }
        } else {
            callback(err, enabled);
        }
    }
    next(null, true);
};

ApiAiRecognizer.prototype.filter = function (context, result, callback) {
    var index = 0;
    var _that = this;

    function next(err, r) {
        if (index < _that._onFilter.length && !err) {
            try {
                _that._onFilter[index++](context, r, next);
            } catch (e) {
                callback(e, null);
            }
        } else {
            callback(err, r);
        }
    }
    next(null, result);
};

module.exports = ApiAiRecognizer;