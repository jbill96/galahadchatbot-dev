var config = {};

// can use separate config objects for each language
// or use dictionary 
config.en = {};
config.ch = {};

config.en.names = {};
config.ch.names = {};

config.en.speak = {};
config.ch.speak = {};

config.en.colors = {};
config.ch.colors = {};

config.en.items = {};
config.ch.items = {};

config.en.sizes = {};
config.ch.sizes = {};

config.en.seasonal = 'Seasonal';
config.en.casual = 'Casual';
config.en.professional = 'Work';
config.en.athletic = 'Exercise';
config.en.formal = 'Formal';

config.en.elegant = 'Elegant';
config.en.urban = 'Urban Style';
config.en['urban style'] = 'Urban Style'
config.en['city style'] = 'Urban Style'
config.en.classic = 'Classic';
config.en.minimalism = 'Minimalism';
config.en.none = 'No obsession';

config.en.like = 'Like';
config.en.dislike = 'Dislike';

config.en.daily = 'Daily';
config.en.weekly = 'Weekly';
config.en.monthly = 'Monthly';
config.en.occasionally = 'Occasionally';

config.en.names.jenny = 'Jenny';
config.en.names.julia = 'Julia';
config.en.names.rebecca = 'Rebecca';
config.en.names.mary = 'Mary';

config.en.sizes = {
    'small' : 'small',
    'medium' : 'medium',
    'large' : 'large'
};

config.en.colors = {
    'blue' : 'blue',
    'green' : 'green',
    'black' : 'black',
    'white' : 'white',
    'yellow' : 'yellow',
    'purple' : 'purple'
};

config.en.items = {
    'tops' : 'tops',
    'shirts' : 'shirts',
    'jackets' : 'jackets',
    'shoes' : 'shoes',
    'dress' : 'dress',
    'pants' : 'pants',
    'sweater' : 'sweater'
};

config.en.orless = ' or less';
config.en.existing_profile_prompt = 'This is your existing Fashion Profile:';
config.en.existing_profile_speak = 'You have an existing fashion profile in our system.';
config.en.favorite_brands = 'Favorite Brands:';
config.en.fashion_obsession = 'Obsession';
config.en.budget = 'Budget:';
config.en.update_question = 'Would you like to update your fashion profile or receive suggestions based on your existing profile?';
config.en.update_profile_text = 'Update Profile';
config.en.existing_profile_text = 'Use Existing Profile';
config.en.shopping_today_question = 'Thanks for updating your profile! What are you shopping for today?';
config.en.recommend_existing_text = 'Ok, I will give you a recommendation based on your existing profile. Please tell me what you like..';
config.en.found_outfit = 'I found an outfit I think you would like!  What do you think?';
config.en.budget_question = 'Do you have a budget in mind? - You can say any monetary amount of less, for example 50 or less '
config.en.brand_question = 'What are some of your favorite brands?'
config.en.change_order_to = 'OK. I have changed your order to ';
config.en.what_color = 'What color are you looking for? ';
config.en.what_size = 'What size are you looking for? ';
config.en.and = ' and ';
config.en.size = ' size ';
config.en.yes_item_size = 'Yes, the %s do come in %s';
config.en.add_cart_question = 'Would you like me to add the %s to your cart';
config.en.sales_support_followup = "The %s %s are available in %s color and size %s. Would you like me to add them to your cart?"
config.en.select = 'Select';
config.en.favorite_brands_return_text = 'I understand! Your favorite brands are %s';
config.en.generate_recommendation_text = 'I am generating recommendations for you...';

config.en.speak.look_at_fashion_profile = 'Take a look at your existing fashion profile';
config.en.speak.update_question = 'Would you like to update your fashion profile or recieve suggestions based on your existing profile? You can say update profile or use existing profile';
config.en.speak.type_choices = 'You can choose casual, work, seasonal, exercise, or formal';
config.en.speak.persona_choices = 'You can choose elegant, urban style, minimalism, classic or none';
config.en.speak.recurring_choices = 'You can choose weekly, daily, monthly and occasionally';


/*************************************/

config.ch.seasonal = '季节性';
config.ch.casual = '休闲';
config.ch.professional = '工作';
config.ch.athletic = '运动';
config.ch.formal = '正式';

config.ch.elegant = '优雅';
config.ch.urban = '城市风格';
config.ch['urban style'] = '城市风格'
config.ch['city style'] = '城市风格'
config.ch.classic = '经典';
config.ch.minimalism = '极简主义';
config.ch.none = '没有痴迷';

config.ch.like = '喜欢';
config.ch.dislike = '不喜欢';

config.ch.daily = '每日';
config.ch.weekly = '每周';
config.ch.monthly = '每月';
config.ch.occasionally = '偶尔';

config.ch.names.jenny = '珍妮';
config.ch.names.julia = '朱莉娅';
config.ch.names.rebecca = '丽贝卡';
config.ch.names.mary = '玛丽';

// need different translation for big
config.ch.sizes = {
    'small' : '小',
    'medium' : '中',
    'large' : '大',
    '5th' : '五',
    '5' : '五',
    '5th and a half' :'五号半',
    '6th' :  '六',
    '6' :  '六',
    '6th and a half': '六号半',
    '7th' : '七',
    '7' : '七',
    '7th and a half' : '七号半',
    '8th' : '八',
    '8' : '八',
    '8th and a half' :'八号半',
    '9th' : '九',
    '9' : '九',
    '9th and a half' : '九号半',
    '10th' : '十',
    '10' : '十',
    '10th and a half' : '十号半',
    '11th' :  '十一',
    '11' :  '十一',
    '11th and a half' : '十一号半'
};

// need chinese translations
config.ch.colors = {
    'blue' : '蓝色',
    'green' : '绿色',
    'black' : '黑色',
    'white' : '白色',
    'yellow' : '黄色',
    'purple' : '紫色',
    'brown' : '棕色',
    'red' : '红'
};

config.ch.items = {
    'tops' : '上衣',
    'shirts' : '衬衫',
    'jackets' : '夹克',
    'shoes' : '鞋',
    'dress' : '连衣裙',
    'pants' : '裤子',
    'trousers' : '裤子',
    'sweater' : '毛线衣',
    'sandals' : '凉鞋',
    'sandal' : '凉鞋',
    'coat' : '外套'
};

// need to change these to chinese 
config.ch.orless = '以下';
config.ch.existing_profile_prompt = '这是您现有的时尚资料';
config.ch.existing_profile_speak = '我们的系统中有一份您现有的时尚资料';
config.ch.favorite_brands = '最喜欢的品牌:';
config.ch.fashion_obsession = '迷恋:';
config.ch.budget = '預算:';
config.ch.update_question = '您想更新您的时尚个人资料，还是根据您现有的时尚个人资料收到建议?';
config.ch.update_profile_text = '更新时尚资料';
config.ch.existing_profile_text = '现有的个人资料';
config.ch.shopping_today_question = '謝謝你更新你的时尚资料，你今天想买什么?';
config.ch.recommend_existing_text = '好的，我会根据你现有的个人资料给你一个建议。请告诉我你喜不喜欢?';
config.ch.found_outfit = '我发现一套我想你会喜欢的衣服！你怎么看?';
config.ch.budget_question = '你心中的预算是多少? 举例来说，你可以說你的預算是500以下 或是200以下'
config.ch.brand_question = '你最喜欢的品牌有哪些?'
config.ch.change_order_to = '好的，我将你的定單改成';
config.ch.what_color = '你想要什么颜色?';
config.ch.what_size = '好的，你需要什么尺寸的？';
config.ch.and = ' 和 ';
config.ch.size = ' 尺码 ';
config.ch.yes_item_size = '是的，這款 %s 有提供 %s 尺寸';
config.ch.add_cart_question = '你要我把這 %s 放到你的购物车吗?';
config.ch.sales_support_followup = '这个%s %s 有%s 颜色和尺寸 %s。您是否希望我将这些商品放到您的购物车?'
config.ch.select = '選擇';
config.ch.favorite_brands_return_text = '了解！您最喜爱的品牌有 %s';
config.ch.generate_recommendation_text = '我正在为你产生建议';

config.ch.speak.look_at_fashion_profile = '請看看你现在的时尚资料';
config.ch.speak.update_question = '您想更新您的时尚个人资料，还是根据您现有的个人资料收到建议？您可以说更新配置文件，或使用现有配置文件。';
config.ch.speak.type_choices = '您可以选择休闲，工作，季节性，运动或正式';
config.ch.speak.persona_choices = '你可以选择优雅，城市风格，极简主义，经典或沒有';
config.ch.speak.recurring_choices = '你可以选择每周，每天，每月和偶尔';


module.exports = config;