const mstranslator = require('mstranslator');
var client = new mstranslator({api_key: process.env.TRANSLATOR_KEY},true)

function translate(params, callback) {
    console.log('TRANSLATING');
    client.translate(params, (err, data) => {
        if (err) {
            callback(err);
        }
        else {
            console.log(data);
            callback(err,data.toLowerCase(),params.from);
        }
    });
}

function translate_array (intent,lang, callback) { 
    console.log('TRANSLATE ARRAY METHOD');
    texts = intent.messages;
    var params = {
        texts : texts,
        from : 'en',
        to : lang
    };
    if (texts.length == 1){
        console.log(texts);
        params = {
            text: intent.messages[0],
            from : 'en',
            to : lang
        };
        client.translate(params, (err, data) => {
            if (err) {
                callback(err);
            }
            else {
                var messages = [];
                messages.push(data);
                console.log(messages);
                intent.messages = messages;
                callback(err, intent);
            }
        })
    }
    else{
        client.translateArray(params, (err, data) => {
            if (err) {
                console.log(err)
                callback(err);
            }
            else {
                var messages = [];
                data.forEach( (element) => {
                    messages.push(element.TranslatedText);
                });
                intent.messages = messages;
                callback(err, intent);
            }
        });
    }
}

function detect(text, callback) {
    console.log(text);
    text = text.toLowerCase()
    var params = { text: [text]};
    client.detect(params, (err, data)=> {
        if(err) {
            callback(err);
        }
        else {
            console.log('DETECTING');
            if(text === 'no' || text === 'hi' || text === 'Hi'){
                data = 'en';
            }
            callback(err, data);
        }
    });
}

module.exports = {
    translate : translate,
    detect: detect,
    translate_array: translate_array
}